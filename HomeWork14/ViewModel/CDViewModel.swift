//
//  CDViewModel.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import CoreData

class CDViewModel: CDViewModelType {
    
    private var persistentContainer: NSPersistentContainer = {
          let container = NSPersistentContainer(name: "HomeWork14")
          container.loadPersistentStores(completionHandler: { (storeDescription, error) in
              if let error = error as NSError? {
                  print("Unresolved error \(error), \(error.userInfo)")
              }
          })
          return container
      }()
    
    var tasks = [CDTask]()
    
    func numberOfRows() -> Int {
        return tasks.count
    }
    
    func taskText(atIndexPath indexPath: IndexPath) -> String {
        return tasks[indexPath.row].value(forKey: "text") as! String
    }
    
    func saveTask(text: String) {
        let task = CDTask(context: persistentContainer.viewContext)
        task.text = text
        task.date = Date()
        do {
            try persistentContainer.viewContext.save()
        } catch {
            print(error.localizedDescription)
        }
        
        tasks.insert(task, at: 0)
    }
    
    func deleteTask(atIndexPath indexPath: IndexPath) {
        let task = tasks[indexPath.row]
        persistentContainer.viewContext.delete(task)
        do {
            try persistentContainer.viewContext.save()
        } catch {
            print(error.localizedDescription)
        }
        
        tasks.remove(at: indexPath.row)
    }
    
    func fetchTasks() {
        tasks.removeAll()
        let fetchRequest: NSFetchRequest<CDTask> = CDTask.fetchRequest()
        do {
            tasks = try persistentContainer.viewContext.fetch(fetchRequest).sorted {$0.date ?? Date() > $1.date ?? Date() }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func cellViewModel(for indexPath: IndexPath) -> CDCellViewModelType? {
        let task = tasks[indexPath.row]
        return CDCellViewModel(task: task)
    }
}
