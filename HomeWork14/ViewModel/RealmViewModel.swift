//
//  RealmViewModel.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import Foundation
import RealmSwift

class RealmViewModel: RealmViewModelType {
    
    private let realm = try! Realm()
    
    var tasks = [RealmTask]()
    
    func numberOfRows() -> Int {
        return tasks.count
    }
    
    func taskText(atIndexPath indexPath: IndexPath) -> String {
        return tasks[indexPath.row].text
    }
    
    func saveTask(text: String) {
        let task = RealmTask(text: text)
        try! realm.write({
            realm.add(task)
        })
        tasks.insert(task, at: 0)
    }
    
    func deleteTask(atIndexPath indexPath: IndexPath) {
        let task = tasks[indexPath.row]
        tasks.remove(at: indexPath.row)
        try! realm.write({
            realm.delete(task)
        })
    }
    
    func fetchTasks() {
        self.tasks.removeAll()
        let tasks = realm.objects(RealmTask.self)
        var result = [RealmTask]()
        for i in tasks {
            result.append(i)
        }
        self.tasks = result.sorted { $0.date > $1.date }
    }
    
    func cellViewModel(for indexPath: IndexPath) -> RealmCellViewModelType? {
        let task = tasks[indexPath.row]
        return RealmCellViewModel(task: task)
    }
}
