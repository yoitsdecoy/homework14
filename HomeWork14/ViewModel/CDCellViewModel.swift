//
//  CDCellViewModel.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import Foundation

class CDCellViewModel: CDCellViewModelType {
    
    private var task: CDTask
    
    var taskText: String {
        return task.text ?? ""
    }
    
    init(task: CDTask) {
        self.task = task
    }
}
