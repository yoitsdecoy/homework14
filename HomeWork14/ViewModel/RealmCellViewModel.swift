//
//  RealmCellViewModel.swift
//  HomeWork14
//
//  Created by Denis Filippov on 04.04.2022.
//

import Foundation

class RealmCellViewModel: RealmCellViewModelType {
    
    private var task: RealmTask
    
    var taskText: String {
        return task.text
    }
    
    init(task: RealmTask) {
        self.task = task
    }
}
