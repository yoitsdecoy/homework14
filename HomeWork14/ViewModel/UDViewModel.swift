//
//  UDViewModel.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import Foundation

class UDViewModel: UDViewModelType {
    private let userDefaults = UserDefaults.standard
    
    func saveUsername(firstName: String, secondName: String) {
        userDefaults.set(firstName + " " + secondName, forKey: "user")
    }
    
    func fetchUsername() -> String {
        guard let username = userDefaults.object(forKey: "user") as? String else {
            return "Hello stranger!\nName yourself!"
        }
        return "Hello again, " + username + "!"
    }
}
