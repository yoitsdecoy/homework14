//
//  WeatherPresenter.swift
//  HomeWork14
//
//  Created by Denis Filippov on 08.04.2022.
//

import UIKit
import RealmSwift

class WeatherPresenter: WeatherPresentationLogic {
    weak var viewController: WeatherDisplayLogic?
    
    var realmManager = RealmManager()
    
    let dateFormatter: DateFormatter = {
        let dt = DateFormatter()
        dt.locale = Locale(identifier: "en_US")
        return dt
    }()
    
    
    //MARK: - presentData
    func presentData(response: WeatherEnum.Model.Response.ResponseType) {
        
        switch response {
        
        case .presentWeather(let weather, let locality):
            
            var hourlyCells = [RealmHourly]()
            var dailyCells = [RealmDaily]()
            
            // create data for hour cells
            weather.hourly.forEach { item in
                hourlyCells.append(RealmHourly(dt: formattedDate(dateFormat: "HH", date: item.dt), temp: setSign(temp: Int(item.temp)), description: item.weather.first!.description, icon: item.weather.first!.icon))
            }
            hourlyCells.removeLast(24)
            hourlyCells[0].dt = "Today"
            
            //create data for daily cells
            weather.daily.forEach { item in
                dailyCells.append(RealmDaily(dt: formattedDate(dateFormat: "EEEE", date: item.dt), minTemp: setSign(temp: Int(item.temp.min)), maxTemp: setSign(temp: Int(item.temp.max)), icon: item.weather.first!.icon))
            }
            dailyCells[0].dt = "Today"
            
            // create data to minMaxLabel
            
            let maxMinTemp = "max.: \(dailyCells[0].maxTemp), min.: \(dailyCells[0].minTemp)"
            
            let realmCurrentWeather = realmHeaderViewModel(weatherModel: weather, hourlyCells: hourlyCells, maxMinTemp: maxMinTemp, dailyCells: dailyCells, locality: locality)
            
            // send display data to viewController
            viewController?.displayData(viewModel: .displayRealmWeather(currentRealmWeatherViewModel: realmCurrentWeather))
            
        case .presentWeatherFromRealm:
            let model = realmManager.getObjects()
            if let model = model {
                viewController?.displayData(viewModel: .displayRealmWeather(currentRealmWeatherViewModel: model))
            }
        }
    }
    
    // formatting the data for the specified format
    private func formattedDate(dateFormat: String, date: Double) -> String {
        dateFormatter.dateFormat = dateFormat
        let currentDate = Date(timeIntervalSince1970: date)
        let dateTitle = dateFormatter.string(from: currentDate).capitalizingFirstLetter()
        return dateTitle
    }
    
    // add the necessary symbols to the temperature
    private func setSign(temp: Int) -> String {
        var currentTemp: String = ""
        guard temp >= 1 else { currentTemp = "\(temp)º"; return currentTemp }
        currentTemp = "+\(temp)º"
        return currentTemp
    }
    
    // convert data to RealmCurrentWeatherViewModel
    private func realmHeaderViewModel(weatherModel: WeatherResponse, hourlyCells: [RealmHourly], maxMinTemp: String, dailyCells: [RealmDaily], locality: String) -> RealmCurrentWeatherViewModel {
        let realmModel = RealmCurrentWeatherViewModel(dt: formattedDate(dateFormat: "d MMM yyyy HH:mm:ss", date: weatherModel.current.dt),
                                                      locality: locality,
                                                      temp: setSign(temp: Int(weatherModel.current.temp)),
                                                      weatherDescription: weatherModel.current.weather.first?.description ?? "nil",
                                                      icon: weatherModel.current.weather.first?.icon ?? "unknown",
                                                      hourlyWeather: hourlyCells,
                                                      maxMinTemp: maxMinTemp,
                                                      dailyWeather: dailyCells)
        
        realmManager.save(data: realmModel)
        
        return realmModel
    }
}

