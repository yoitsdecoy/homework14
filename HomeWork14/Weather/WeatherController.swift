//
//  WeatherController.swift
//  HomeWork14
//
//  Created by Denis Filippov on 06.04.2022.
//

import Foundation
import UIKit


class WeatherController: UIViewController, WeatherDisplayLogic {
    
    var interactor: WeatherBusinessLogic?
    
    let weatherView = WeatherView()
    
    // MARK: - Setup
    
    private func setup() {
        let viewController        = self
        let interactor            = WeatherInteractor()
        let presenter             = WeatherPresenter()
        viewController.interactor = interactor
        interactor.presenter      = presenter
        presenter.viewController  = viewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        setupViews()
        setup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        interactor?.makeRequest(request: .getWeather)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    //MARK: - displayData
    func displayData(viewModel: WeatherEnum.Model.ViewModel.ViewModelData) {
        
        switch viewModel {
        case .displayRealmWeather(let currentRealmWeatherViewModel):
            weatherView.configure(realmViewModel: currentRealmWeatherViewModel)
        }
    }
    
    func setupViews() {
        var constraints = [NSLayoutConstraint]()
        view.addSubview(weatherView)
        weatherView.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(weatherView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor))
        constraints.append(weatherView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor))
        constraints.append(weatherView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0))
        constraints.append(weatherView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0))
        
        NSLayoutConstraint.activate(constraints)
    }
}
