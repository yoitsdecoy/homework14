//
//  WeatherInteractor.swift
//  HomeWork14
//
//  Created by Denis Filippov on 08.04.2022.
//

import UIKit
import CoreLocation
import RealmSwift

class WeatherInteractor: NSObject, WeatherBusinessLogic, CLLocationManagerDelegate {
    
    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    var presenter: WeatherPresentationLogic?
    var networkManager = NetworkManager()
    var realmManager = RealmManager()
    
    //MARK: - makeRequest
    func makeRequest(request: WeatherEnum.Model.Request.RequestType) {
       
        switch request {
        case .getWeather:
            getLocation()
        }
    }
    
    private func getLocation(){
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }

    //MARK: - locationManager
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        // Get coordinates
        guard let location = locations.last else { return }
        self.locationManager.stopUpdatingLocation()
        let coordinates = "lat=\(location.coordinate.latitude)&lon=\(location.coordinate.longitude)"
        guard let currentLocation = locationManager.location else { return }
        
        // Get location name
        geocoder.reverseGeocodeLocation(currentLocation, preferredLocale: Locale.init(identifier: "en_US")) { placemarks, error in
            let locality = placemarks?[0].locality ?? (placemarks?[0].name ?? "Failure")
            
                // Get Weather
                self.networkManager.getWeather(coordinates: coordinates) { weatherResponse in
                    if let response = weatherResponse {
                        self.presenter?.presentData(response: .presentWeather(weather: response, locality: locality))
                    } else {
                        DispatchQueue.main.async {
                            let realmObject = self.realmManager.getObjects()
                            if let realmObject = realmObject {
                                self.presenter?.presentData(response: .presentWeatherFromRealm(fromObject: realmObject, locality: locality))
                            }
                        }
                    }
                }
        }
    }
}

