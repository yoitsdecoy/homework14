//
//  RealmManager.swift
//  HomeWork14
//
//  Created by Denis Filippov on 09.04.2022.
//

import Foundation
import RealmSwift

class RealmManager {
    
    let realm = try! Realm()
    
    func save(data: Object) {
        DispatchQueue.main.async {
            try! self.realm.write({
                self.realm.deleteAll()
                self.realm.add(data)
            })
        }
        
    }
    
    func getObjects() -> RealmCurrentWeatherViewModel? {
        let result = Array(realm.objects(RealmCurrentWeatherViewModel.self))
        return result.count > 0 ? result[0] : nil
    }
}
