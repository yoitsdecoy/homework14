//
//  API.swift
//  HomeWork14
//
//  Created by Denis Filippov on 06.04.2022.
//

import Foundation

struct API {
    private let apiKey = "926247ac53ed30b659cab42495a479fd"
    static let url = "https://api.openweathermap.org/data/2.5/onecall?exclude=minutely&lang=en&units=metric&appid=926247ac53ed30b659cab42495a479fd&"
}
