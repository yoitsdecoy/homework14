//
//  WeatherBusinessLogic.swift
//  HomeWork14
//
//  Created by Denis Filippov on 09.04.2022.
//

import Foundation

protocol WeatherBusinessLogic {
    func makeRequest(request: WeatherEnum.Model.Request.RequestType)
}
