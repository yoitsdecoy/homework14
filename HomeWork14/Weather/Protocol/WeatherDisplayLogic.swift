//
//  WeatherDisplayLogic.swift
//  HomeWork14
//
//  Created by Denis Filippov on 09.04.2022.
//

import Foundation

protocol WeatherDisplayLogic: AnyObject {
    func displayData(viewModel: WeatherEnum.Model.ViewModel.ViewModelData)
}

