//
//  WeatherPresentationLogic.swift
//  HomeWork14
//
//  Created by Denis Filippov on 09.04.2022.
//

import Foundation

protocol WeatherPresentationLogic {
    func presentData(response: WeatherEnum.Model.Response.ResponseType)
}
