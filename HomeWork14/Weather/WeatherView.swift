//
//  WeatherView.swift
//  HomeWork14
//
//  Created by Denis Filippov on 08.04.2022.
//

import Foundation
import UIKit

class WeatherView: UIView {
    private var mainView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.alpha = 0
        return view
    }()
    
    private var cityLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 35)
        label.textColor = .black
        label.textAlignment = .right
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 0.5
        return label
    }()
    
    private var tempLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 60, weight: .thin)
        label.textColor = .black
        label.textAlignment = .left
        return label
    }()
    
    private var descriptionLabel: UILabel = {
       let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.textAlignment = .right
        return label
    }()
    
    private var lastUpdateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = .black
        label.textAlignment = .right
        return label
    }()
    
    private var maxMinLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18)
        label.textColor = .black
        label.textAlignment = .right
        return label
    }()
    
    private var hourlyCollectionView = HourlyCollectionView()
    private var dailyTableView = DailyTableView()
    
    //MARK: - init
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(mainView)
        mainView.addSubview(cityLabel)
        mainView.addSubview(tempLabel)
        mainView.addSubview(descriptionLabel)
        mainView.addSubview(maxMinLabel)
        mainView.addSubview(lastUpdateLabel)
        mainView.addSubview(hourlyCollectionView)
        mainView.addSubview(dailyTableView)
        makeConstraints()
    }
    
    //MARK: - constraints
    private func makeConstraints(){
        // mainView constraints
        mainView.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        mainView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        mainView.heightAnchor.constraint(equalTo: heightAnchor).isActive = true
        mainView.widthAnchor.constraint(equalTo: widthAnchor).isActive = true
        
        // cityLabel constraints
        cityLabel.topAnchor.constraint(equalTo: mainView.topAnchor).isActive = true
        cityLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -18).isActive = true
        cityLabel.leadingAnchor.constraint(equalTo: tempLabel.trailingAnchor, constant: 10).isActive = true
        
        // tempLabel constraints
        tempLabel.leadingAnchor.constraint(equalTo: mainView.leadingAnchor, constant: 10).isActive = true
        tempLabel.centerYAnchor.constraint(equalTo: descriptionLabel.centerYAnchor).isActive = true
        tempLabel.widthAnchor.constraint(equalToConstant: 130).isActive = true
        
        // descriptionLabel constraints
        descriptionLabel.topAnchor.constraint(equalTo: cityLabel.bottomAnchor).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -18).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        // maxMinLabel constraints
        maxMinLabel.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8).isActive = true
        maxMinLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -18).isActive = true
        maxMinLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        // lastUpdateLabel constraints
        lastUpdateLabel.topAnchor.constraint(equalTo: maxMinLabel.bottomAnchor, constant: 8).isActive = true
        lastUpdateLabel.trailingAnchor.constraint(equalTo: mainView.trailingAnchor, constant: -18).isActive = true
        lastUpdateLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        
        // hourlyCollectionView constraints
        hourlyCollectionView.translatesAutoresizingMaskIntoConstraints = false
        hourlyCollectionView.topAnchor.constraint(equalTo: lastUpdateLabel.bottomAnchor, constant: 10).isActive = true
        hourlyCollectionView.heightAnchor.constraint(equalToConstant: 165).isActive = true
        hourlyCollectionView.leftAnchor.constraint(equalTo: self.leftAnchor).isActive = true
        hourlyCollectionView.rightAnchor.constraint(equalTo: self.rightAnchor).isActive = true
        
        // dailyTableView constraints
        dailyTableView.translatesAutoresizingMaskIntoConstraints = false
        dailyTableView.topAnchor.constraint(equalTo: hourlyCollectionView.bottomAnchor, constant: 10).isActive = true
        dailyTableView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        dailyTableView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 10).isActive = true
        dailyTableView.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -10).isActive = true
    }
    
    //MARK: - configure
    func configure(realmViewModel: RealmCurrentWeatherViewModel) {
        
        var hourly = [RealmHourly]()
        var daily = [RealmDaily]()
        
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.4, delay: 0.0, options: [.allowUserInteraction], animations:
                            { () -> Void in
                                self.mainView.alpha = 1
                            })
            self.backgroundColor = self.mainView.backgroundColor
            self.cityLabel.text = realmViewModel.locality
            self.tempLabel.text = realmViewModel.temp
            self.descriptionLabel.text = realmViewModel.weatherDescription
            self.maxMinLabel.text = realmViewModel.maxMinTemp
            self.lastUpdateLabel.text = "Last update: " + realmViewModel.dt
            
            realmViewModel.hourlyWeather.forEach { i in
                hourly.append(i)
            }
            realmViewModel.dailyWeather.forEach { i in
                daily.append(i)
            }
            self.hourlyCollectionView.set(cells: hourly)
            self.dailyTableView.realmSet(cells: daily)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
