//
//  WeatherModels.swift
//  HomeWork14
//
//  Created by Denis Filippov on 08.04.2022.
//

import Foundation
import RealmSwift

enum WeatherEnum {
    enum Model {
        struct Request {
            enum RequestType {
                case getWeather
            }
        }
        struct Response {
            enum ResponseType {
                case presentWeather(weather: WeatherResponse, locality: String)
                case presentWeatherFromRealm(fromObject: Object, locality: String)
            }
        }
        struct ViewModel {
            enum ViewModelData {
                case displayRealmWeather(currentRealmWeatherViewModel: RealmCurrentWeatherViewModel)
            }
        }
    }
}

class RealmCurrentWeatherViewModel: Object {
    @Persisted var dt: String
    @Persisted var locality: String
    @Persisted var temp: String
    @Persisted var weatherDescription: String
    @Persisted var icon: String
    @Persisted var hourlyWeather: List<RealmHourly>
    @Persisted var maxMinTemp: String
    @Persisted var dailyWeather: List<RealmDaily>

    convenience init(dt: String, locality: String, temp: String, weatherDescription: String, icon: String, hourlyWeather: [RealmHourly], maxMinTemp: String, dailyWeather: [RealmDaily]) {
        self.init()
        self.dt = dt
        self.locality = locality
        self.temp = temp
        self.weatherDescription = weatherDescription
        self.icon = icon
        self.hourlyWeather.append(objectsIn: hourlyWeather)
        self.maxMinTemp = maxMinTemp
        self.dailyWeather.append(objectsIn: dailyWeather)
    }
    
    

}

class RealmDaily: EmbeddedObject {
    @Persisted var dt: String
    @Persisted var minTemp: String
    @Persisted var maxTemp: String
    @Persisted var icon: String

    convenience init(dt: String, minTemp: String, maxTemp: String, icon: String) {
        self.init()
        self.dt = dt
        self.minTemp = minTemp
        self.maxTemp = maxTemp
        self.icon = icon
    }

}

class RealmHourly: EmbeddedObject {
    @Persisted var dt: String
    @Persisted var temp: String
    @Persisted var _description: String
    @Persisted var icon: String

    convenience init(dt: String, temp: String, description: String, icon: String) {
        self.init()
        self.dt = dt
        self.temp = temp
        self._description = description
        self.icon = icon
    }
}
