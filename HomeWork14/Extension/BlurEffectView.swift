//
//  BlurEffectView.swift
//  HomeWork14
//
//  Created by Denis Filippov on 06.04.2022.
//

import Foundation
import UIKit

class BlurEffect: UIVisualEffectView {
    
    override init(effect: UIVisualEffect?) {
        super.init(effect: UIBlurEffect(style: .regular))
        self.translatesAutoresizingMaskIntoConstraints = false
        self.layer.opacity = 0.4
        self.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
