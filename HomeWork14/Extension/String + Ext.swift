//
//  String + Ext.swift
//  HomeWork14
//
//  Created by Denis Filippov on 06.04.2022.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
      return prefix(1).uppercased() + self.lowercased().dropFirst()
    }

}
