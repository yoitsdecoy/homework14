//
//  UIViewController + Ext.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import UIKit

extension UIViewController {
    func showAddTaskDialog(title: String? = nil,
                           subtitle: String? = nil,
                           actionTitle: String? = "Add",
                           cancelTitle: String? = "Cancel",
                           placeholder: String? = nil,
                           inputKeyboardType: UIKeyboardType = UIKeyboardType.default, cancelHandler: ((UIAlertAction) -> Void)? = nil,
                           actionHandler: ((_ text: String?) -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: subtitle, preferredStyle: .alert)
        alert.addTextField { textField in
            textField.placeholder = placeholder
            textField.keyboardType = inputKeyboardType
        }
        alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { action in
            guard let textField = alert.textFields?.first else {
                actionHandler?(nil)
                return
            }
            actionHandler?(textField.text)
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .cancel, handler: cancelHandler))
        self.present(alert, animated: true)
    }
}
