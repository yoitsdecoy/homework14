//
//  RealmTask.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import Foundation
import RealmSwift

class RealmTask: Object {
    @Persisted var text: String
    @Persisted var date: Date = Date()
    
    convenience init(text: String) {
        self.init()
        self.text = text
    }
}
