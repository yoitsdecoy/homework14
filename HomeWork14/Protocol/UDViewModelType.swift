//
//  UDViewModelType.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import Foundation

protocol UDViewModelType {
    func saveUsername(firstName: String, secondName: String)
    func fetchUsername() -> String
}
