//
//  RealmCellViewModelType.swift
//  HomeWork14
//
//  Created by Denis Filippov on 04.04.2022.
//

import Foundation

protocol RealmCellViewModelType: AnyObject {
    var taskText: String { get }
}
