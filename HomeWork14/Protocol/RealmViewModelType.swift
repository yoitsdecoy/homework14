//
//  RealmViewModelType.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import Foundation

protocol RealmViewModelType {
    func numberOfRows() -> Int
    func taskText(atIndexPath indexPath: IndexPath) -> String
    func cellViewModel(for indexPath: IndexPath) -> RealmCellViewModelType?
    func saveTask(text: String)
    func deleteTask(atIndexPath indexPath: IndexPath)
    func fetchTasks()
}
