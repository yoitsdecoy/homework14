//
//  CDCellViewModelType.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import Foundation

protocol CDCellViewModelType: AnyObject {
    var taskText: String { get }
}
