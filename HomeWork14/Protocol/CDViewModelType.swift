//
//  CDViewModelType.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import Foundation

protocol CDViewModelType {
    func numberOfRows() -> Int
    func taskText(atIndexPath indexPath: IndexPath) -> String
    func cellViewModel(for indexPath: IndexPath) -> CDCellViewModelType?
    func saveTask(text: String)
    func deleteTask(atIndexPath indexPath: IndexPath)
    func fetchTasks()
}
