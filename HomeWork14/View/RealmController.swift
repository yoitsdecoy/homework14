//
//  RealmController.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import UIKit

class RealmController: UITableViewController {
    
    private var viewModel: RealmViewModelType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        viewModel = RealmViewModel()
        tableView.register(RealmCell.self, forCellReuseIdentifier: RealmCell.id)
        
        viewModel?.fetchTasks()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(didTapAdd))
        navigationController?.navigationBar.tintColor = .black
    }
    
    @objc private func didTapAdd() {
        showAddTaskDialog(title: "New Task", subtitle: nil, actionTitle: "Save task", cancelTitle: "Cancel", placeholder: "Type some task", inputKeyboardType: .default, cancelHandler: nil) { text in
            guard let text = text else { return }
            self.viewModel?.saveTask(text: text)
            self.tableView.reloadData()
        }


    }
    
}

// MARK: - UITableViewDataSource

extension RealmController {
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel?.numberOfRows() ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RealmCell.id) as! RealmCell
        cell.viewModel = viewModel?.cellViewModel(for: indexPath)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        40
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel?.deleteTask(atIndexPath: indexPath)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
}


