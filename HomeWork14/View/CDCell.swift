//
//  CDCell.swift
//  HomeWork14
//
//  Created by Denis Filippov on 05.04.2022.
//

import UIKit

class CDCell: UITableViewCell {
    static var id = "CDCell"
    
    var label = UILabel()
    
    weak var viewModel: CDCellViewModelType? {
        willSet(viewModel) {
            guard let viewModel = viewModel else { return }
            label.text = viewModel.taskText
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        configureTitleLabel()
    }
    
    private func configureTitleLabel() {
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        var constraints = [NSLayoutConstraint]()
        constraints.append(label.topAnchor.constraint(equalTo: self.topAnchor))
        constraints.append(label.bottomAnchor.constraint(equalTo: self.bottomAnchor))
        constraints.append(label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -20))
        constraints.append(label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20))
        NSLayoutConstraint.activate(constraints)
        label.font = UIFont(name: "Avenir Next", size: 16)
        label.textColor = .black
        label.textAlignment = .left
        
    }
}
