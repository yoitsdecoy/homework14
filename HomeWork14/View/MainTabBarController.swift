//
//  MainTabBarController.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import UIKit

class MainTabBarController: UITabBarController {
    override func viewDidLoad() {
            setupControllers()
            tabBar.backgroundColor = .white
            tabBar.tintColor = .black
        }
        
        private func setupControllers() {
            let userDefaultsController = UDController()
            userDefaultsController.title = "User Defaults"
            userDefaultsController.tabBarItem = UITabBarItem(title: userDefaultsController.title, image: UIImage(systemName: "circle"), selectedImage: UIImage(systemName: "circle.fill"))
            
            let realmController = RealmController()
            realmController.title = "Realm"
            realmController.tabBarItem = UITabBarItem(title: realmController.title, image: UIImage(systemName: "circle"), selectedImage: UIImage(systemName: "circle.fill"))
            
            let coreDataController = CDController()
            coreDataController.title = "Core Data"
            coreDataController.tabBarItem = UITabBarItem(title: coreDataController.title, image: UIImage(systemName: "circle"), selectedImage: UIImage(systemName: "circle.fill"))
            
            let weatherController = WeatherController()
            weatherController.title = "Weather"
            weatherController.tabBarItem = UITabBarItem(title: weatherController.title, image: UIImage(systemName: "circle"), selectedImage: UIImage(systemName: "circle.fill"))
            
            
            viewControllers = [
                UINavigationController(rootViewController: userDefaultsController),
                UINavigationController(rootViewController: realmController),
                UINavigationController(rootViewController: coreDataController),
                UINavigationController(rootViewController: weatherController)
            ]
        }
}
