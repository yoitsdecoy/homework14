//
//  UDController.swift
//  HomeWork14
//
//  Created by Denis Filippov on 03.04.2022.
//

import UIKit

class UDController: UIViewController {
    
    private var viewModel: UDViewModelType?
    
    private var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = UIFont(name: "Avenir Next Bold", size: 24)
        return label
    }()
    
    private var firstNameTF: UITextField = {
        let textField = UITextField()
        
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter your First Name"
        
        return textField
    }()
    
    private var secondNameTF: UITextField = {
        let textField = UITextField()
        
        textField.borderStyle = .roundedRect
        textField.placeholder = "Enter your Second Name"
        
        return textField
    }()
    
    private var button: UIButton = {
        let config = UIButton.Configuration.tinted()
        let button = UIButton(configuration: config)
        button.setTitle("OK", for: UIControl.State.normal)
        button.tintColor = .black
        
        return button
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let text = viewModel?.fetchUsername()
        guard let text = text else { return }
        label.text = text
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        viewModel = UDViewModel()
        
        setupViews()
        
        let text = viewModel?.fetchUsername()
        guard let text = text else { return }
        label.text = "Hello again, " + text + "!"
    }
    
    private func setupViews() {
        var constraints = [NSLayoutConstraint]()
        
        // label
        
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(label.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor))
        constraints.append(label.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16))
        constraints.append(label.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16))
        constraints.append(label.heightAnchor.constraint(equalToConstant: 250))
        
        // firstNameTF
        view.addSubview(firstNameTF)
        firstNameTF.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(firstNameTF.topAnchor.constraint(equalTo: label.bottomAnchor, constant: 16))
        constraints.append(firstNameTF.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16))
        constraints.append(firstNameTF.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16))
        constraints.append(firstNameTF.heightAnchor.constraint(equalToConstant: 44))
        
        // secondNameTF
        view.addSubview(secondNameTF)
        secondNameTF.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(secondNameTF.topAnchor.constraint(equalTo: firstNameTF.bottomAnchor, constant: 16))
        constraints.append(secondNameTF.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 16))
        constraints.append(secondNameTF.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -16))
        constraints.append(secondNameTF.heightAnchor.constraint(equalToConstant: 44))
        
        // button
        view.addSubview(button)
        button.translatesAutoresizingMaskIntoConstraints = false
        constraints.append(button.topAnchor.constraint(equalTo: secondNameTF.bottomAnchor, constant: 16))
        constraints.append(button.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        button.addAction(UIAction(handler: { _ in
            guard let firstName = self.firstNameTF.text, let secondName = self.secondNameTF.text else { return }
            if firstName != "", secondName != "" {
                self.viewModel?.saveUsername(firstName: firstName, secondName: secondName)
                self.label.text = "Hello, \(firstName) \(secondName)!"
            } else {
                self.label.text = "Please, fill all the fields below!"
            }
        }), for: .touchUpInside)
        
        NSLayoutConstraint.activate(constraints)
    }
}
